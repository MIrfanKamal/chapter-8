﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour
{
    public float speedBoost;
    public float boostTime;
    public float spawnTime;

    private void Awake()
    {
        StartCoroutine(SpawnTime());
    }

    private void Update()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * 20, 0));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.isTrigger == false)
        {
            PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();
            playerMovement.BoostSpeed(boostTime, speedBoost);
            Destroy(gameObject);
        }
    }

    IEnumerator SpawnTime()
    {
        yield return new WaitForSeconds(spawnTime);
        Destroy(gameObject);
    }
}
