﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    public int healthGain;
    public float spawnTime;

    private void Awake()
    {
        StartCoroutine(SpawnTime());
    }

    private void Update()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * 20, 0));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.isTrigger == false)
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            playerHealth.TakeHealth(healthGain);
            Destroy(gameObject);
        }
    }

    IEnumerator SpawnTime()
    {
        yield return new WaitForSeconds(spawnTime);
        Destroy(gameObject);
    }
}
